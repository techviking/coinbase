module coinbase

go 1.13

require (
	github.com/gorilla/websocket v1.4.1
	gitlab.com/techviking/fixed v1.0.0
)
