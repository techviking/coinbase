package coinbase

import (
	"time"

	"gitlab.com/techviking/fixed"
)

type (
	Product struct {
		ID             string      `json:"id"`              // "BTC-USD",
		BaseCurrency   string      `json:"base_currency"`   // "BTC"
		QuoteCurrency  string      `json:"quote_currency"`  // "USD"
		BaseMinSize    fixed.Fixed `json:"base_min_size"`   //  "0.001"
		BaseMaxSize    fixed.Fixed `json:"base_max_size"`   // "10000.00"
		QuoteIncrement fixed.Fixed `json:"quote_increment"` // "0.01"
	}

	// intentionally a type alias
	ProductsResponse = []Product

	ProductTicker struct {
		TradeID int `json:"trade_id"` // 4729088,
		// TODO convert these to use robaho/fixed
		Price  fixed.Fixed `json:"price"`  //  "333.99"
		Size   fixed.Fixed `json:"size"`   // "0.193"
		Bid    fixed.Fixed `json:"bid"`    // "333.98"
		Ask    fixed.Fixed `json:"ask"`    // "333.99"
		Volume fixed.Fixed `json:"volume"` //  "5957.11914015"
		Time   time.Time   `json:"time"`   //  "2015-11-14T20:46:03.511254Z"
	}
)
