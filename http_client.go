package coinbase

import (
	"fmt"
	"net/http"
	"time"
)

var (
	httpClient *http.Client
)

func init() {
	httpClient = &http.Client{
		Transport: http.DefaultTransport,
		Timeout:   time.Second * 30, // Go 1.3
	}
}

// sendRequest - Add request to the proper queue for processing.
func  sendPublicRequest(request *http.Request) (*http.Response, error) {
	var resChan chan *queueResponse

		publicRequestMutex.Lock()
		resChan = publicRequestQueue.add(request)
		publicRequestMutex.Unlock()
		go processPublicQueue()

	res := <-resChan

	return res.response, res.err
}

func sendPrivateRequest(request *http.Request) (*http.Response, error) {
	var resChan chan *queueResponse

	privateRequestMutex.Lock()
	resChan = privateRequestQueue.add(request)
	privateRequestMutex.Unlock()
	go processPrivateQueue()

	res := <-resChan

	return res.response, res.err
}

func  sendRequest(entry *requestQueueEntry) {
	resp, err := httpClient.Do(entry.req)

	if err != nil {
		entry.resChan <- &queueResponse{
			response: nil,
			err:      err,
		}

		return
	}

	// @TODO - handle 429 errors here.  currently panic to prevent being banned.
	if resp.StatusCode == 429 {
		panic(
			fmt.Sprintf(
				"429 response detected, rate limiter needs work.  Also here's the body: %+v",
				resp.Body,
			),
		)
	}

	entry.resChan <- &queueResponse{
		response: resp,
		err:      err,
	}
}

