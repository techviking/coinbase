package coinbase

type Interval = int

const (
	OneMin Interval = 60
	FiveMin Interval = 300
	FifteenMin Interval = 900
	OneHour Interval = 3600
	SixHour Interval = 21600
	OneDay Interval = 86400
)
