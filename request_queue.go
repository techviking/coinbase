package coinbase

import (
	"container/list"
	"fmt"
	"net/http"
	"sync"
)

type (
	requestQueueEntry struct {
		req     *http.Request
		resChan chan *queueResponse
	}

	requestQueue struct {
		l *list.List
	}

	queueResponse struct {
		response *http.Response
		err      error
	}
)

func (rq *requestQueue) add(req *http.Request) chan *queueResponse {
	if rq.l == nil {
		rq.l = list.New()

	}

	resChan := make(chan *queueResponse)

	rq.l.PushBack(&requestQueueEntry{
		req:     req,
		resChan: resChan,
	})

	return resChan
}

func (rq *requestQueue) shift() (*requestQueueEntry, bool) {

	if rq.l == nil {
		return nil, false
	}

	initialLen := rq.l.Len()

	entry := rq.l.Remove(rq.l.Front()).(*requestQueueEntry)

	// apparently can't remove the last element from a List, so instead blowing it away.
	if initialLen == 1 {
		rq.l = nil
	}

	return entry, true
}


var (
	publicRequestQueue requestQueue
	publicRequestMutex sync.Mutex
	publicQueueProcessing bool

  privateRequestQueue requestQueue
	privateRequestMutex sync.Mutex
	privateQueueProcessing bool
)

func init() {
	publicRequestQueue = requestQueue{}
	privateRequestQueue = requestQueue{}
}



func processPublicQueue() {
	if publicQueueProcessing {
		return
	}

	publicQueueProcessing = true
	for {
		publicRequestMutex.Lock()
		entry, ok := publicRequestQueue.shift()
		publicRequestMutex.Unlock()

		if !ok {
			publicQueueProcessing = false
			return
		}

		<- publicThrottle
		go sendRequest(entry)
	}
}

func processPrivateQueue() {
	if privateQueueProcessing {
		return
	}

	privateQueueProcessing = true

	for {
		privateRequestMutex.Lock()
		entry, ok := privateRequestQueue.shift()
		privateRequestMutex.Unlock()

		if !ok {
			privateQueueProcessing = false
			return
		}

		<- privateThrottle

		timestamp := timestampSec()

		sig, err := generateSigFromRequest(
			timestamp,
			entry.req,
		)

		if err != nil {
			socketChan <- []byte(
				fmt.Sprintf("{\"message\":\"%s\"}", err.Error()),
			)
			continue
		}

		entry.req.Header = authHeaders.Clone()
		entry.req.Header.Add("CB-ACCESS-TIMESTAMP", timestamp)
		entry.req.Header.Add("CB-ACCESS-SIGN", sig)

		go sendRequest(entry)
	}
}
