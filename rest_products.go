package coinbase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func GetProducts() (ProductsResponse, error) {
	targetUrl := *activeEndpoints.restUrl
	targetUrl.Path = "/products"

	resp, err := sendPublicRequest(
		&http.Request{
			Method: http.MethodGet,
			URL:    &targetUrl,
		},
	)

	if err != nil {
		return nil, err
	}

	var (
		bodyBuffer *bytes.Buffer
		respBody   ProductsResponse
	)

	bodyBuffer = new(bytes.Buffer)
	if _, err = bodyBuffer.ReadFrom(resp.Body); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bodyBuffer.Bytes(), respBody); err != nil {
		return nil, err
	}

	return respBody, nil
}

func GetProductTicker(market string) (*ProductTicker, error) {
	targetUrl := *activeEndpoints.restUrl
	targetUrl.Path = fmt.Sprintf("/products/%s/ticker", market)

	resp, err := sendPublicRequest(
		&http.Request{
			Method: http.MethodGet,
			URL:    &targetUrl,
		},
	)

	if err != nil {
		return nil, err
	}

	var (
		bodyBuffer *bytes.Buffer
		respBody   ProductTicker
	)

	bodyBuffer = new(bytes.Buffer)
	if _, err = bodyBuffer.ReadFrom(resp.Body); err != nil {
		return nil, err
	}

	if err := json.Unmarshal(bodyBuffer.Bytes(), &respBody); err != nil {
		return nil, err
	}

	return &respBody, nil
}
