package coinbase

import (
	"strconv"
	"time"
)

func timestampSec() string {
	return strconv.Itoa(
		int(time.Now().Unix()),
	)
}
