package coinbase

import (
	"net/http"
	"testing"
)

func TestRequestQueue(t *testing.T) {
	var (
		testQueue requestQueue
		req *http.Request
	)

	req, _ = http.NewRequest(
		"GET",
		"http://localhost",
		nil)

	testQueue.add(req)

	thing, ok := testQueue.shift()

	if !ok || thing == nil {
		t.Errorf("first shift should have given us what we added: %+v - %+v", ok, thing)
	}

	thing2, ok2 := testQueue.shift()

	if ok2 || thing2 != nil {
		t.Errorf("second shift should have revealed an empty list: %+v, %+v", ok, thing2)
	}

}
