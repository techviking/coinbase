package coinbase

func SubscribeToHeartbeat(markets []string) error {
	return sendWebsocketMessage(
		map[string]interface{}{
			"type": "subscribe",
			"product_ids": markets,
			"channels": []string{
				"heartbeat",
			},
		},
		false,
	)
}
